package input;

import java.util.HashMap;

import javafx.scene.input.KeyCode;
import runtime.RuntimeVariables;

public class Input {
	private static HashMap<KeyCode, Boolean> currentlyActiveKeys = new HashMap<>();

	public static boolean IsKeyPressed(int numberOfKeys, KeyCode... keyCode) {
		RuntimeVariables.RuntimeFrameProperties.getScene().setOnKeyPressed(event -> {
			currentlyActiveKeys.put(event.getCode(), true);
		});

		RuntimeVariables.RuntimeFrameProperties.getScene().setOnKeyReleased(event -> {
			currentlyActiveKeys.clear();
		});

		if (keyCode.length >= numberOfKeys && !currentlyActiveKeys.isEmpty()) {

			for (int i = 0; i < numberOfKeys; i++) {
				if (currentlyActiveKeys.get(keyCode[i]) == null || currentlyActiveKeys.get(keyCode[i]) == false) {
					return false;
				}else {
					currentlyActiveKeys.put(keyCode[i], false);
				}
			}
		} else {
			return false;
		}

		return true;
	}
}
