package runtime;

import frame.FrameProperties;
import javafx.animation.Timeline;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class RuntimeVariables {
	public static class RuntimeFrameProperties {
		private static FrameProperties frameProperties;
		private static Stage stage;
		private static Group group;
		private static Scene scene;
		private static Timeline timeline;

		public static void SetRuntimeFrameProperties(FrameProperties _frameProperties, Stage _stage, Group _group, Scene _scene, Timeline timeine) {
			RuntimeFrameProperties.stage = _stage;
			RuntimeFrameProperties.group = _group;
			RuntimeFrameProperties.scene = _scene;
			RuntimeFrameProperties.frameProperties = _frameProperties;
		}

		public static FrameProperties getFrameProperties() {
			return frameProperties;
		}

		public static void setFrameProperties(FrameProperties frameProperties) {
			RuntimeFrameProperties.frameProperties = frameProperties;
		}

		public static Stage getStage() {
			return stage;
		}

		public static void setStage(Stage stage) {
			RuntimeFrameProperties.stage = stage;
		}

		public static Group getGroup() {
			return group;
		}

		public static void setGroup(Group group) {
			RuntimeFrameProperties.group = group;
		}

		public static Scene getScene() {
			return scene;
		}

		public static void setScene(Scene scene) {
			RuntimeFrameProperties.scene = scene;
		}

		public static Timeline getTimeline() {
			return timeline;
		}

		public static void setTimeline(Timeline timeline) {
			RuntimeFrameProperties.timeline = timeline;
		}
		
	}
}
