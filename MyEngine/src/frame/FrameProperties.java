package frame;

import javafx.scene.paint.Color;

public class FrameProperties {
	private double width;
	private double height;
	private String title;
	private Color backgroundColor;
	
	private double fps;
	
	private boolean autoUpdate = false;
	
	public FrameProperties(double _width, double _height, String _title, Color _backgroundColor, double fps) {
		this.width = _width;
		this.height = _height;
		this.title = _title;
		this.backgroundColor = _backgroundColor;
		
		this.fps = fps;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Color getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public double getFps() {
		return fps;
	}

	public void setFps(double fps) {
		this.fps = fps;
	}
}
