package frame;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;
import runtime.RuntimeSoftObjects;
import runtime.RuntimeVariables;

public abstract class FrameBehavior {

	private FrameProperties frameProperties;
	public FrameParameters frameParameters = new FrameParameters();
	private Stage stage = new Stage();
	private Group group = new Group();
	private Scene scene;
	private Timeline timeline;

	public abstract void OnGameInitialized();

	public abstract void OnGameStarted();

	public abstract void OnGameStoped();

	public abstract void GraphicUpdate();

	public abstract void PhysicUpdate();

	public void Start(FrameProperties frameProperties) {
		this.frameProperties = frameProperties;

		OnGameInitialized();
		frameParameters.isRunning = true;

		scene = new Scene(group, frameProperties.getWidth(), frameProperties.getHeight());
		scene.setFill(frameProperties.getBackgroundColor());
		stage.setTitle(frameProperties.getTitle());
		stage.setScene(scene);
		stage.show();

		SetRuntimeFrameVariables();

		StartListeiner();

		OnGameStarted();

		Loop();
	}

	public void Loop() {
		final Duration oneFrameAmt = Duration.millis(1000 / frameProperties.getFps());
		final KeyFrame frame = new KeyFrame(oneFrameAmt, (javafx.event.ActionEvent event) -> {
			if (frameParameters.isRunning) {
				GraphicUpdate();
				PhysicUpdate();
				
				if(frameParameters.autoUpdate == true) {
					for (int i = 0; i < RuntimeSoftObjects.runtimeObjects.size(); i++) {
						RuntimeSoftObjects.runtimeObjects.get(i).Update();
					}
				}
			}
		});
		timeline = new Timeline(frame);
		timeline.setCycleCount(Timeline.INDEFINITE);
		timeline.playFromStart();
	}

	private void SetRuntimeFrameVariables() {
		RuntimeVariables.RuntimeFrameProperties.SetRuntimeFrameProperties(frameProperties, stage, group, scene,
				timeline);
	}

	private void StartListeiner() {
		stage.setOnCloseRequest(event -> {
			OnGameStoped();

			frameParameters.isRunning = false;
		});
	}
}
