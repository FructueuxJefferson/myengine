package game;

import frame.FrameBehavior;
import frame.FrameProperties;
import game.obj.Obj1;
import javafx.application.Application;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import runtime.RuntimeSoftObjects;
import runtime.RuntimeVariables;

public class Game extends Application {

	private FrameProperties frameProperties = new FrameProperties(1280, 720, "Game Engine", Color.BLACK, 60);
	MainFrame mainFrame = new MainFrame();
	Obj1 square = new Obj1();

	@Override
	public void start(Stage primaryStage) throws Exception {
		mainFrame.frameParameters.autoUpdate = true;
		RuntimeSoftObjects.runtimeObjects.add(square);
		mainFrame.Start(frameProperties);
	}

	public class MainFrame extends FrameBehavior {

		@Override
		public void OnGameInitialized() {
			System.out.println("Init");
			
			square.width = 75;
			square.height = 75;
		}

		@Override
		public void OnGameStarted() {
			System.out.println("Start");
			square.Start();
			square.Instantiate();
		}

		@Override
		public void OnGameStoped() {
			System.out.println(RuntimeVariables.RuntimeFrameProperties.getStage().getTitle());
			System.out.println("Quit");
		}

		@Override
		public void GraphicUpdate() {
			//System.out.println("GraphicTick");
		}		

		@Override
		public void PhysicUpdate() {
			//square.Update();
		}
	}

}
