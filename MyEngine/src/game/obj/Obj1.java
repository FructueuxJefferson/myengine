package game.obj;

import input.Input;
import javafx.scene.input.KeyCode;
import objects.Square;
import vector.Vector2;

public class Obj1 extends Square {

	@Override
	public void Start() {
		transform.position = new Vector2(500, 300);
		Instantiate();
		System.out.println("OK");
	}

	@Override
	public void Update() {
		if (Input.IsKeyPressed(1, KeyCode.D)) {
			SetPosition(new Vector2(transform.position.X + 1, transform.position.Y));
		}
		if (Input.IsKeyPressed(1, KeyCode.Q)) {
			SetPosition(new Vector2(transform.position.X - 1, transform.position.Y));
		}
	}

	@Override
	public void OnObjectSpawned() {
		// TODO Auto-generated method stub

	}
}
