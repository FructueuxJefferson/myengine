package graphics;

import java.awt.*;

import runtime.RuntimeVariables;

@SuppressWarnings("exports")
public class RelativeSpace {

	public static GraphicsEnvironment graphicsEnvironment;
	public static Rectangle window;
	public static Point center;

	public RelativeSpace() {
		graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
		window = graphicsEnvironment.getMaximumWindowBounds();
		center = graphicsEnvironment.getCenterPoint();
	}

	public GraphicsEnvironment getGraphicsEnvironment() {
		return graphicsEnvironment;
	}

	public static Rectangle getWindow() {
		return window;
	}

	public static Point getCenter() {
		return center;
	}

	public void setWindow(Rectangle window) {
		this.window = window;
	}

	public void setCenter(Point center) {
		this.center = center;
	}

	public static double getRelativeWidth(double dWidth) {
		double x = 1920 / dWidth;

		return RuntimeVariables.RuntimeFrameProperties.getFrameProperties().getWidth() / x;
	}

	public static double getRelativeHeight(double dHeight) {
		double y = 1080 / dHeight;

		return RuntimeVariables.RuntimeFrameProperties.getFrameProperties().getHeight() / y;
	}
}
