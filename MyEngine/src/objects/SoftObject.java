package objects;

import vector.Vector2;

public interface SoftObject {
	public abstract void Start();

	public abstract void Update();

	public abstract void SetActive();

	public abstract void OnObjectSpawned();

	public abstract void SetPosition(Vector2 newPosition);
	
	public abstract void Instantiate();
}
