package objects;

import javafx.scene.Parent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import objects.transform.Transform;
import runtime.RuntimeVariables;
import vector.Vector2;;

public abstract class Square extends Parent implements SoftObject {
	private Rectangle gameObject;
	public Transform transform = new Transform();
	public float width, height;
	public boolean isActive = false;

	public Square() {
		
	}

	@Override
	public abstract void Update();

	@Override
	public abstract void OnObjectSpawned();

	@Override
	public void SetActive() {
		this.getChildren().remove(gameObject);
	}

	@Override
	public void SetPosition(Vector2 newPosition) {
		transform.position = newPosition;

		gameObject.setTranslateX(newPosition.X);
		gameObject.setTranslateY(newPosition.Y);
	}

	@Override
	public void Instantiate() {
		if (RuntimeVariables.RuntimeFrameProperties.getGroup().getChildren().contains(gameObject)) {
			RuntimeVariables.RuntimeFrameProperties.getGroup().getChildren().remove(gameObject);
		}
		
		gameObject = new Rectangle(width * transform.scale.X, height * transform.scale.Y, Color.WHITE);

		SetPosition(transform.position);
		RuntimeVariables.RuntimeFrameProperties.getGroup().getChildren().add(gameObject);
	}
	
	
	
	
	public Rectangle GameObject() {
		return gameObject;
	}

}
