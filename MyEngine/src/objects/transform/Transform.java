package objects.transform;

import vector.Vector2;

public class Transform {
	public Vector2 position;
	public Vector2 rotation;
	public Vector2 scale;
	
	public Transform(Vector2 poisition, Vector2 rotation, Vector2 scale) {
		this.position = poisition;
		this.rotation = rotation;
		this.scale = scale;
	}
	
	public Transform() {
		this.position = new Vector2(0, 0);
		this.rotation = new Vector2(0, 0);
		this.scale = new Vector2(1, 1);
	}
}
